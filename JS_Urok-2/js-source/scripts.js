document.addEventListener('DOMContentLoaded', function(){

  document.querySelectorAll('.share-project .normal-button')[0].onclick = () => {
    console.log('Share project button');
    saveButtonClickEvent();
    document.querySelectorAll('.site-header, .color-selector-layout, .calendars-selector').forEach((currentTag) => 
    {
      currentTag.className += ' _blur';
      });

    document.querySelector('.popup-overlay').className += ' -flex';
    document.querySelector('.popup-window').className += ' -show';
  };


  function getUnicId(arg1) {
    const dateString = Math.round(Math.random() * 100, 0) + arg1.toString(30);
    return dateString;
  };


  function updateURL(unicId) {
    const currentURL = window.location.origin + window.location.pathname;
    let updatedURL = currentURL + "?id=" + unicId;
    return updatedURL;
  };


  function replaceURL(newURL) {
    window.history.replaceState(null, null, newURL);
    return true;
  };


  function pushURLtoLinkplace(newURL) {
    document.querySelector('.linkplace').innerHTML = newURL;
    return true;
  };


  function isIdPresent() {
  	let presentId = window.location.href.search('id=');
  	return presentId;
  };


  function saveButtonClickEvent() {

  	if (isIdPresent()) {
  		const dateTime = Date.now();
      const unicId = getUnicId(dateTime);
      const newURL = updateURL(unicId);
      replaceURL(newURL);
  	}
    pushURLtoLinkplace(newURL);
    return true;
  };

});
