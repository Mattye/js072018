document.addEventListener('DOMContentLoaded', function(){
	
	document.querySelectorAll('.share-project .normal-button')[0].onclick = () => {
		console.log('Share project button');
		saveButtonClickEvent();
		document.querySelectorAll('.site-header, .color-selector-layout, .calendars-selector').forEach((currentTag) => 
		{
			currentTag.className += ' _blur';
		});

		document.querySelector('.popup-overlay').className += ' -flex';
		document.querySelector('.popup-window').className += ' -show';


	};



	function getUnicId(arg1) {
		const dateString = Math.round(Math.random()*100, 0) + arg1.toString(30);
		return dateString;
	}


	function updateURL(unicId) {
		const currentURL = window.location.origin + window.location.pathname;
		let updatedURL = currentURL + "?id=" + unicId;
		return updatedURL;
	}


	function replaceURL(newURL) {
		window.history.replaceState(null, null, newURL);
		return true;
	}


	function pushURLtoLinkplace(newURL) {
		document.querySelector('.linkplace').innerHTML = newURL;
		return true;
	}

	
	function saveButtonClickEvent() {
		const dateTime = Date.now();
		const unicId = getUnicId(dateTime);
		const newURL = updateURL(unicId);
		replaceURL(newURL);
		pushURLtoLinkplace(newURL);
		return true;
	}
	
/*
	1. По клику:
	* 1.1. Присваиваем переменной дату момента клика, парсим её в 30ю систему;
	* 1.1.1. Присовокупляем рандомные 2 числа к дате;	  
	* 1.2. Присваиваем переменной текущую строку URL и присовокупляем дату;
	  1.3. Вводим в строку URL браузера полученную строку URL из переменной;
	  1.4. Добавляем в класс .linkplace строку из переменной 1.2.
*/
});