document.addEventListener('DOMContentLoaded', function() {

  let months = [
    {
      id: '201808',
      name: 'August',
      days: [
        [0, 0, 1, 2, 3, 4, 5],
        [6, 7, 8, 9, 10, 11, 12],
        [13, 14, 15, 16, 17, 18, 19],
        [20, 21, 22, 23, 24, 25, 26],
        [27, 28, 29, 30, 31, 0, 0],
        [0, 0, 0, 0, 0, 0, 0]
      ],
      markedDays: [1, 5, 8]
    }
  ];


  let data = months;


  const removeClass = (whereToRemove, whatToRemove) => {
    document.querySelector(whereToRemove).classList.remove(whatToRemove);
  };


  const addClass = (whereToAdd, whatToAdd) => {
    document.querySelector(whereToAdd).className += whatToAdd;
  };


  const addCalendar = (event) => {
    console.log('Add command detected on:');
    console.log(event.target);

    let predMonthId = event.target.closest('.section-month').dataset.calendarId;
    let monthTemplate = document.querySelector('.calendars-selector .section-month');

    let additionalMonth = {
    id: '201801',
    name: 'January',
    days: [
      [0, 0, 0, 0, 1, 2, 3],
      [4, 5, 6, 7, 8, 9, 10],
      [11, 12, 13, 14, 15, 16, 17],
      [18, 19, 20, 21, 22, 23, 24],
      [25, 26, 27, 28, 29, 30, 31],
      [0, 0, 0, 0, 0, 0, 0]
    ],
    markedDays: [0, 0, 0]
    };

    switch(predMonthId) {
      case '201801': 
        {
          additionalMonth.id = '201802';
          additionalMonth.name = 'February';
        };
        break;
      case '201802': 
        {
          additionalMonth.id = '201803';
          additionalMonth.name = 'March';
        };
        break;
      case '201803': 
        {
          additionalMonth.id = '201804';
          additionalMonth.name = 'April';
        };
        break;
      case '201804': 
        {
          additionalMonth.id = '201805';
          additionalMonth.name = 'May';
        };
        break;
      case '201805': 
        {
          additionalMonth.id = '201806';
          additionalMonth.name = 'June';
        };
        break;
      case '201806': 
        {
          additionalMonth.id = '201807';
          additionalMonth.name = 'July';
        };
        break;
      case '201807': 
        {
          additionalMonth.id = '201808';
          additionalMonth.name = 'August';
        };
        break;
      case '201808': 
        {
          additionalMonth.id = '201809';
          additionalMonth.name = 'September';
        };
        break;
      case '201809': 
        {
          additionalMonth.id = '201810';
          additionalMonth.name = 'October';
        };
        break;
      case '201810': 
        {
          additionalMonth.id = '201811';
          additionalMonth.name = 'November';
        };
        break;
      case '201811': 
        {
          additionalMonth.id = '201812';
          additionalMonth.name = 'December';
        };
        break;
      case '201812': 
        {
          additionalMonth.id = '201901';
          additionalMonth.name = 'January';
        };
        break;
      default: break;
    }; 

    data.push(additionalMonth);
    
    reGenerateCalendars(data, monthTemplate);
  };


  const deleteCalendar = (event) => {
    console.log('Delete command detected:');
    console.log(event.target);
    let monthsData = data;
    let deleteMonthId = event.target.closest('.section-month').dataset.calendarId;
    let monthTemplate = document.querySelector('.calendars-selector .section-month');

    monthsData.forEach((item, index) => {
      if (item.id === deleteMonthId) {
        monthsData.splice(index, 1);
      }
    });
 
    console.log(monthsData);

    reGenerateCalendars(monthsData, monthTemplate);
  };


  const reGenerateCalendars = (calendarsDataBase, calendarTemplate) => {
    let monthTemplate = document.querySelector('.calendars-selector .flexwrapper');
    monthTemplate.innerHTML = '';
    calendarsDataBase.forEach((item, index) => {
      const oneMonth = createSingleMonth(item, calendarTemplate);
      monthTemplate.appendChild(oneMonth.cloneNode(true));
    });
    
    document.querySelectorAll('.month-selector .deletemonth').forEach(deleteButtonListener);
    document.querySelectorAll('.month-selector .addmonth').forEach(addButtonListener);
  };


  const getCurrentColor = () => {
    const colorList = {
      red: '#ff9797',
      blue: '#97b7ff',
      yellow: '#151515',
      green: 'green',
      grey: 'grey'
    };

    return colorList[$('.active-color-selector .dropdown').val()];
  };


  $('.calendar td').on('click', (event) => {
    console.log('clicked');
  });

  console.log(getCurrentColor());


  const addButtonListener = (item, index) => {
    item.addEventListener('click', addCalendar);
  };
  

  const deleteButtonListener = (item, index) => {
    item.addEventListener('click', deleteCalendar);
  };


  document.querySelectorAll('.share-project .normal-button')[0].onclick = () => {
    saveButtonClickEvent();
    addClass('.popup-overlay', ' -flex');
    addClass('.popup-window', ' -show');
  };


  document.querySelector('.popup-overlay .normal-button').onclick = function() {
    removeClass('.popup-window', '-show');
    removeClass('.popup-overlay', '-flex');
  };


  function getUnicId(arg1) {
    const dateString = Math.round(Math.random()*100, 0) + arg1.toString(30);
    return dateString;
  };


  function updateURL(unicId) {
    const currentURL = window.location.origin + window.location.pathname;
    let updatedURL = currentURL + "?id=" + unicId;
    return updatedURL;
  };


  function replaceURL(newURL) {
    window.history.replaceState(null, null, newURL);
    return true;
  };


  function pushURLtoLinkplace(newURL) {
    document.querySelector('.linkplace').innerHTML = newURL;
    return true;
  };


  function isIdPresent() {
    let presentId = window.location.href.search('id=');
    return presentId;
  };


  function saveButtonClickEvent() {
    if (isIdPresent() > -1) {
      pushURLtoLinkplace(window.location.href);
      return;
    }
    const dateTime = Date.now();
    const unicId = getUnicId(dateTime);
    const newURL = updateURL(unicId);
    replaceURL(newURL);
    pushURLtoLinkplace(newURL);
    return true;
  };


  const createSingleMonth = (argDays, argTemplate) => {
    argTemplate.setAttribute('data-calendar-id', argDays.id);
    argTemplate.querySelector('.month-selector .title').innerHTML = argDays.name;
    let dayTemplate = argTemplate.querySelectorAll('.calendar .days .day');
    let dayNumber = 0; 

    for (let i = 0; i < argDays.days.length; i++) {

      let oneWeek = argDays.days[i];

      for (let day = 0; day < oneWeek.length; day++) {
        dayTemplate[dayNumber].innerHTML = oneWeek[day];
        dayNumber = dayNumber + 1;
      }

    }
  return argTemplate;
  };


  const generateCalendar = (argDataMonths) => {
    let monthTemplate = document.querySelector('.calendars-selector .section-month');
    let monthContainer = document.querySelector('.calendars-selector .flexwrapper');

    document.querySelector('.calendars-selector .section-month').remove();
    console.log('Исходный Календарь удалён.');

    for (let i = 0; i < argDataMonths.length; i++) {
      let oneMonth = createSingleMonth(argDataMonths[i], monthTemplate.cloneNode(true));            
      
      console.log('Generated month is: ');
      console.log(oneMonth);

      monthContainer.appendChild(oneMonth);
    }
  };


  generateCalendar(data);

  document.querySelectorAll('.month-selector .deletemonth').forEach(deleteButtonListener);
  document.querySelectorAll('.month-selector .addmonth').forEach(addButtonListener);

});