document.addEventListener('DOMContentLoaded', function() {

  let months = [
    {
      id: '201801',
      name: 'January',
      days: [
        [0, 0, 0, 0, 0, 0, 1],
        [2, 3, 4, 5, 6, 7, 8],
        [9, 10, 11, 12, 13, 14, 15],
        [16, 17, 18, 19, 20, 21, 22],
        [23, 24, 25, 26, 27, 28, 29],
        [30, 31, 0, 0, 0, 0, 0]
      ],
      markedDays: [1, 5, 8]
    },
    {
      id: '201802',
      name: 'February',
      days: [
        [0, 0, 1, 2, 3, 4, 5],
        [6, 7, 8, 9, 10, 11, 12],
        [13, 14, 15, 16, 17, 18, 19],
        [20, 21, 22, 23, 24, 25, 26],
        [27, 28, 29, 30, 31, 0, 0],
        [0, 0, 0, 0, 0, 0, 0]
      ],
      markedDays: [2, 6, 17]
    },
    {
      id: '201803',
      name: 'March',
      days: [
        [0, 0, 0, 0, 0, 1, 2],
        [3, 4, 5, 6, 7, 8, 9],
        [10, 11, 12, 13, 14, 15, 16],
        [17, 18, 19, 20, 21, 22, 23],
        [24, 25, 26, 27, 28, 29, 30],
        [0, 0, 0, 0, 0, 0, 0]
      ],
      markedDays: null
    }
  ];


  let data = months;


  console.log('Whole massive:');
  console.log(data);
  console.log('One month:');
  console.log(data[1].days);

  
  const removeClass = (whereToRemove, whatToRemove) => {
    document.querySelector(whereToRemove).classList.remove(whatToRemove);
  };


  const addClass = (whereToAdd, whatToAdd) => {
    document.querySelector(whereToAdd).className += whatToAdd;
  };


  const addCalendar = () => {
    console.log('Month added.');
  };


  const regenerateCalendars = (calendars, argTemplate) => {
    let monthTemplate = document.querySelector('.calendars-selector .flexwrapper');
    monthTemplate.innerHTML = '';
    calendars.forEach((item, index) => {
      const oneMonth = createSingleMonth(item, argTemplate);
      monthTemplate.appendChild(oneMonth.cloneNode(true));
    });
    
    document.querySelectorAll('.month-selector .deletemonth').forEach(deleteButtonListener);
    document.querySelectorAll('.month-selector .addmonth').forEach(addButtonListener);
  };

  const deleteCalendar = (event) => {
    console.log('Delete command detected:');
    console.log(event.target);
    let monthsData = data;
    let deleteMonthId = event.target.closest('.section-month').dataset.calendarId;
    let monthTemplate = document.querySelector('.calendars-selector .section-month');

    monthsData.forEach((item, index) => {
      if (item.id === deleteMonthId) {
        monthsData.splice(index, 1);
      }
    });
 
    console.log(monthsData);

    regenerateCalendars(monthsData, monthTemplate);

  };


  const addButtonListener = (item, index) => {
    item.addEventListener('click', addCalendar);
  };
  

  const deleteButtonListener = (item, index) => {
    item.addEventListener('click', deleteCalendar);
  };


  document.querySelectorAll('.share-project .normal-button')[0].onclick = () => {
    saveButtonClickEvent();
    addClass('.popup-overlay', ' -flex');
    addClass('.popup-window', ' -show');
  };


  document.querySelector('.popup-overlay .normal-button').onclick = function() {
    removeClass('.popup-window', '-show');
    removeClass('.popup-overlay', '-flex');
  };


  function getUnicId(arg1) {
    const dateString = Math.round(Math.random()*100, 0) + arg1.toString(30);
    return dateString;
  };


  function updateURL(unicId) {
    const currentURL = window.location.origin + window.location.pathname;
    let updatedURL = currentURL + "?id=" + unicId;
    return updatedURL;
  };


  function replaceURL(newURL) {
    window.history.replaceState(null, null, newURL);
    return true;
  };


  function pushURLtoLinkplace(newURL) {
    document.querySelector('.linkplace').innerHTML = newURL;
    return true;
  };


  function isIdPresent() {
    let presentId = window.location.href.search('id=');
    return presentId;
  };


  function saveButtonClickEvent() {
    if (isIdPresent() > -1) {
      pushURLtoLinkplace(window.location.href);
      return;
    }
    const dateTime = Date.now();
    const unicId = getUnicId(dateTime);
    const newURL = updateURL(unicId);
    replaceURL(newURL);
    pushURLtoLinkplace(newURL);
    return true;
  };


  const createSingleMonth = (argDays, argTemplate) => {
    argTemplate.setAttribute('data-calendar-id', argDays.id);
    argTemplate.querySelector('.month-selector .title').innerHTML = argDays.name;
    let dayTemplate = argTemplate.querySelectorAll('.calendar .days .day');
    let dayNumber = 0; 

    for (let i = 0; i < argDays.days.length; i++) {

      let oneWeek = argDays.days[i];

      for (let day = 0; day < oneWeek.length; day++) {
        dayTemplate[dayNumber].innerHTML = oneWeek[day];
        dayNumber = dayNumber + 1;
      }

    }
  return argTemplate;
  };


  const generateCalendar = (argDataMonths) => {
    let monthTemplate = document.querySelector('.calendars-selector .section-month');
    let monthContainer = document.querySelector('.calendars-selector .flexwrapper');

    document.querySelector('.calendars-selector .section-month').remove();
    console.log('Исходный Календарь удалён.');

    for (let i = 0; i < argDataMonths.length; i++) {
      let oneMonth = createSingleMonth(argDataMonths[i], monthTemplate.cloneNode(true));            
      
      console.log('Generated month is: ');
      console.log(oneMonth);

      monthContainer.appendChild(oneMonth);
    }
  };


  generateCalendar(data);

  document.querySelectorAll('.month-selector .deletemonth').forEach(deleteButtonListener);
  document.querySelectorAll('.month-selector .addmonth').forEach(addButtonListener);

});