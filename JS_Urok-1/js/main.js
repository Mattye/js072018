function getNumbersInput() {
  let firstInput = document.querySelector('.numberone').value;
  let secondInput = document.querySelector('.numbertwo').value;

  let bothInputs = [+firstInput, +secondInput];

  return bothInputs;
}


function multiply(firstValue, secondValue) {
  let multiplResult = firstValue * secondValue;

  return multiplResult;
}


function displayResult(resultMlt) {
  document.querySelector('.result').innerHTML = 'Result = ' + resultMlt;
}


function clickEventButton() {
  let values = getNumbersInput();
  let multResult = multiply(values[0], values[1]);
  
  displayResult(multResult);
}


document.querySelector('.calculate').onclick = clickEventButton;